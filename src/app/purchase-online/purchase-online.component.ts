import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs/tabset.component';
import { DataServiceService } from '../data-service.service';
import { IProduct } from './product';

@Component({
  selector: 'app-purchase-online',
  templateUrl: './purchase-online.component.html',
  styleUrls: ['./purchase-online.component.scss'],
})
export class PurchaseOnlineComponent implements OnInit {
  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;

  newArrivals: IProduct[] = [];
  bestSellers: IProduct[] = [];
  mostViews: IProduct[] = [];
  colors: string[] = [];
  activeTab = 1;

  constructor(private dataService: DataServiceService) {}

  ngOnInit(): void {
    this.loadNewArrivals();
    this.loadMostViews();
    this.loadBestSellers();
  }

  loadNewArrivals(color?: string) {
    this.dataService.loadNewArrivals(color).subscribe((data) => {
      if (color && color.length !== 0 && color !== 'All Color') {
        this.newArrivals = this.getNewDataFiltred(data, color);
      } else {
        this.getColors(data);
        this.newArrivals = data;
      }
    });
  }

  loadBestSellers(color?: string) {
    this.dataService.loadBestSellers(color).subscribe((data) => {
      if (color && color.length !== 0 && color !== 'All Color') {
        this.bestSellers = this.getNewDataFiltred(data, color);
      } else {
        this.getColors(data);
        this.bestSellers = data;
      }
    });
  }

  loadMostViews(color?: string) {
    this.dataService.loadMostViews(color).subscribe((data) => {
      if (color && color.length !== 0 && color !== 'All Color') {
        this.mostViews = this.getNewDataFiltred(data, color);
      } else {
        this.getColors(data);
        this.mostViews = data;
      }
    });
  }

  filterColor(color: string) {
    switch (this.activeTab) {
      case 1: {
        this.loadNewArrivals(color);
        break;
      }
      case 2: {
        this.loadBestSellers(color);
        break;
      }
      case 3: {
        this.loadMostViews(color);
        break;
      }
    }
  }

  selectTab(tabId: number) {
    this.activeTab = tabId;
  }

  private getColors(data: IProduct[]) {
    let allColors: string[] = [];
    data.map((product) => {
      allColors.push(...product.color);
    });
    this.colors = ['All Color', ...new Set(allColors)];
  }

  private getNewDataFiltred(data: IProduct[], color: string) {
    let newData: IProduct[] = [];
    data.map((product) => {
      if (product.color.includes(color)) {
        newData.push(product);
      }
    });
    return newData;
  }
}
