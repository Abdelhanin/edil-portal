export interface IProduct {
  imageThumb: string;
  imagePreview: string;
  name: string;
  description: string;
  price: string;
  oldPrice: string;
  discount: string;
  link: string;
  color: string[];
}
