import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IProduct } from './purchase-online/product';

@Injectable({
  providedIn: 'root',
})
export class DataServiceService {
  constructor(private httpClient: HttpClient) {}

  loadNewArrivals(color?: string) {
    return this.httpClient.get<IProduct[]>('assets/data/new_arrivals.json');
  }

  loadBestSellers(color?: string) {
    return this.httpClient.get<IProduct[]>('assets/data/most_view.json');
  }

  loadMostViews(color?: string) {
    return this.httpClient.get<IProduct[]>('assets/data/best_seller.json');
  }
}
