import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  categories: string[] = [
    'Arredo',
    'Bagno',
    'Cucina',
    'Outdoor',
    'Ufficio',
    'Contract',
    'Illuminazione',
    'Welness',
    'Decor',
    'Finiture,',
    'Edilizia',
    'Tech',
  ];

  constructor() {}

  ngOnInit(): void {}
}
